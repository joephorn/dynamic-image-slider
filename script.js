var currentImage = 0
var images = ["url('resources/nature1.jpg')", "url('resources/nature2.jpg')", "url('resources/nature3.jpg')", "url('resources/nature4.jpg')"];

function ChangeImage(count){
    currentImage = count;

    if(currentImage < 0){
        currentImage = 3;
    }
    if(currentImage > 3){
        currentImage = 0;
    }

    document.getElementById('image').style.backgroundImage = images[currentImage];
}